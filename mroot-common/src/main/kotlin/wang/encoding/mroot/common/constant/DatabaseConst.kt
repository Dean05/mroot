/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.constant


import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

import javax.validation.constraints.NotBlank


/**
 * 数据库的配置变量名称
 *
 * @author ErYang
 */
@Configuration
@PropertySource("classpath:database.properties")
@ConfigurationProperties(prefix = "database")
@EnableCaching
class DatabaseConst {

    /**
     * 后台分页条数 名称
     */
    @NotBlank
    lateinit var adminPageSizeName: String

    /**
     * 七牛cdn地址 名称
     */
    @NotBlank
    lateinit var qiniuCdnName: String

    /**
     * 上传 名称
     */
    @NotBlank
    lateinit var uploadName: String

    /**
     * 上传七牛的值 名称
     */
    @NotBlank
    lateinit var uploadQiniuName: String

}

// -----------------------------------------------------------------------------------------------------

// End DatabaseConst class

/* End of file DatabaseConst.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/constant/DatabaseConst.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
