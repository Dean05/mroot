/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.config

import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import wang.encoding.mroot.common.filter.XssFilter


/**
 * XSS 拦截配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
class XssConfiguration {

    /**
     * xss 过滤拦截器
     */
    @Bean
    fun xssFilterRegistrationBean(): FilterRegistrationBean<XssFilter> {
        val filterRegistrationBean: FilterRegistrationBean<XssFilter> = FilterRegistrationBean(XssFilter())
        filterRegistrationBean.order = 1
        filterRegistrationBean.isEnabled = true
        filterRegistrationBean.addUrlPatterns("/*")
        // 过滤
        val initParameters = HashMap<String, String>()
        initParameters["excludes"] = "/*/favicon.ico,static/*,/assets/*,/verifyImage"
        initParameters["isIncludeRichText"] = "true"
        filterRegistrationBean.initParameters = initParameters
        return filterRegistrationBean
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End XssConfiguration class

/* End of file XssConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/config/XssConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
