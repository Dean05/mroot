/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util


/**
 * 测试工具类
 *
 * @author ErYang
 */
object TestUtil {


    /**
     * 数字金额大写转换，思想先写个完整的然后将如零拾替换成零
     * 要用到正则表达式
     */
    private fun moneyUppercase(money: Double): String {
        var n: Double = money
        val fraction: Array<String> = arrayOf("角", "分")
        val digit: Array<String> = arrayOf("零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖")
        val unit: Array<Array<String>> = arrayOf(arrayOf("元", "万", "亿"), arrayOf("", "拾", "佰", "仟"))

        val head: String = if (n < 0) "负" else ""
        n = Math.abs(n)

        var s = ""
        for (i: Int in fraction.indices) {
            s += (digit[(Math.floor(n * 10.0 * Math.pow(10.0, i.toDouble())) % 10).toInt()] + fraction[i]).replace("(零.)+".toRegex(), "")
        }
        if (s.isEmpty()) {
            s = "整"
        }
        var integerPart: Int = Math.floor(n).toInt()

        var i = 0
        while (i < unit[0].size && integerPart > 0) {
            var p = ""
            var j = 0
            while (j < unit[1].size && n > 0) {
                p = digit[integerPart % 10] + unit[1][j] + p
                integerPart /= 10
                j++
            }
            s = p.replace("(零.)*零$".toRegex(), "").replace("^$".toRegex(), "零") + unit[0][i] + s
            i++
        }
        return head + s.replace("(零.)*零元".toRegex(), "元").replaceFirst("(零.)+".toRegex(), "").replace("(零.)+".toRegex(), "零").replace("^整$".toRegex(), "零元整")
    }

    /**
     * 测试
     *
     * @param args
     * @throws Exception
     */
    @JvmStatic
    fun main(args: Array<String>) {

        val s1 = "123456"
        println(s1.toBigIntegerOrNull())
        val s2 = "1234ss56"
        println(s2.toBigIntegerOrNull())

        //整数
        println(moneyUppercase(100000.0))

        // for循环每隔5秒循环一次
        for (i: Int in 0..30) {
            try {
                val time: Long = System.currentTimeMillis()
                println(time.toString() + " i:" + i)
                //设置暂停的时间，5000=5秒
                Thread.sleep(5000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }


        }


    }

    // -------------------------------------------------------------------------------------------------


}

// -----------------------------------------------------------------------------------------------------

// End TestUtil class

/* End of file TestUtil.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/util/TestUtil.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
