/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util

import freemarker.template.Configuration
import freemarker.template.Template
import freemarker.template.TemplateException
import wang.encoding.mroot.common.exception.BaseException
import java.io.*
import java.util.*

/**
 * Freemarker 工具类
 * @author ErYang
 */
object FreemarkerUtil {


    private const val ENCODING = "UTF-8"


    // -----------------------------------------------------------------------------------------------------

    /**
     * 打印到控制台(测试用)
     *
     * @param ftlName
     */
    @Throws(BaseException::class)
    fun print(ftlName: String, root: Map<String, Any>, ftlPath: String) {
        try {
            //通过Template可以将模板文件输出到相应的流
            val temp: Template? = getTemplate(ftlName, ftlPath)
            temp?.process(root, PrintWriter(System.out))
        } catch (e: TemplateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 输出到输出到文件
     *
     * @param ftlName  ftl文件名
     * @param root     传入的 map
     * @param outFile  输出后的文件全部路径
     * @param filePath 输出前的文件上部路径
     */
    @Throws(BaseException::class)
    fun printFile(ftlName: String, root: Map<String, Any>,
                  outFile: String, filePath: String,
                  ftlPath: String) {
        try {
            val file = File(PathUtil.classpath + filePath + outFile)
            // 判断有没有父路径，就是判断文件整个路径是否存在
            if (!file.parentFile.exists()) {
                // 不存在就全部创建
                file.parentFile.mkdirs()
            }
            val out = BufferedWriter(OutputStreamWriter(FileOutputStream(file), ENCODING))
            val template: Template? = getTemplate(ftlName, ftlPath)
            //模版输出
            template?.process(root, out)
            out.flush()
            out.close()
        } catch (e: TemplateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 通过文件名加载模版
     *
     * @param name    文件名
     * @param ftlPath 路径
     * @return 模板
     * @throws Exception
     */
    @Throws(BaseException::class)
    private fun getTemplate(name: String, ftlPath: String): Template? {
        try {
            // 通过 freemarker 的Configuration读取相应的ftl
            val cfg = Configuration(Configuration.VERSION_2_3_27)
            cfg.setEncoding(Locale.CHINA, ENCODING)
            // 设定去哪里读取相应的模板文件
            cfg.setDirectoryForTemplateLoading(File(PathUtil.classpath + ftlPath))
            // 在模板文件目录中找到名称为name的文件
            return cfg.getTemplate(name)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }

    // -----------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End FreemarkerUtil class

/* End of file FreemarkerUtil.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/util/FreemarkerUtil.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
