/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.config


import com.qiniu.common.Zone
import com.qiniu.storage.Configuration
import com.qiniu.storage.UploadManager
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.EnableCaching
import org.springframework.stereotype.Component
import wang.encoding.mroot.common.constant.QiNiuConst
import wang.encoding.mroot.common.util.QiNiuUtil

import javax.annotation.PostConstruct


/**
 * 七牛配置
 *
 * @author ErYang
 */
@Component
@EnableCaching
class QiNiuConfiguration {


    companion object {

        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(QiNiuConfiguration::class.java)
    }

    @Autowired
    private lateinit var qiNiuConst: QiNiuConst

    /**
     * 注入配置
     */
    @PostConstruct
    fun setQiNiuUtil() {
        if (logger.isDebugEnabled) {
            logger.debug(">>>>>>>>设置七牛工具类的配置开始>>>>>>>>")
        }
        QiNiuUtil.ACCESS_KEY = qiNiuConst.accessKey
        QiNiuUtil.SECRET_KEY = qiNiuConst.secretKey
        QiNiuUtil.ZONE = getQiniuZone(qiNiuConst.zone)
        QiNiuUtil.BUCKET = qiNiuConst.bucket
        QiNiuUtil.BASE_URL = qiNiuConst.baseUrl
        QiNiuUtil.configuration = getConfiguration(getQiniuZone(qiNiuConst.zone))
        QiNiuUtil.uploadManager = getUploadManager(getQiniuZone(qiNiuConst.zone))
        if (logger.isDebugEnabled) {
            logger.debug(">>>>>>>>设置七牛工具类的配置" +
                    "[ACCESS_KEY={},SECRET_KEY={}" +
                    ",ZONE={},BUCKET={}," +
                    "BASE_URL={}]>>>>>>>>", qiNiuConst.accessKey, qiNiuConst.secretKey,
                    qiNiuConst.zone, qiNiuConst.bucket, qiNiuConst.baseUrl)
            logger.debug(">>>>>>>>设置七牛工具类的配置结束>>>>>>>>")
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到七牛机房地址
     *
     * @return Zone
     */
    private fun getQiniuZone(zone: String): Zone {
        // 华东：zone0 华北：zone1 华南：zone2 北美：zoneNa0
        return when (zone) {
            "zone0" -> Zone.zone0()
            "zone1" -> Zone.zone1()
            "zone2" -> Zone.zone2()
            "zoneNa0" -> Zone.zoneNa0()
            else -> Zone.autoZone()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 Configuration
     *
     * @param zone Zone
     * @return Configuration
     */
    private fun getConfiguration(zone: Zone): Configuration {
        return Configuration(zone)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 UploadManager
     *
     * @param zone Zone
     * @return UploadManager
     */
    private fun getUploadManager(zone: Zone): UploadManager {
        val configuration: Configuration = getConfiguration(zone)
        return UploadManager(configuration)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QiNiuConfiguration class

/* End of file QiNiuConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/config/QiNiuConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
