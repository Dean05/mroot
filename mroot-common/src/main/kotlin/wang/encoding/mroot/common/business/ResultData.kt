/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.business


import com.alibaba.fastjson.JSONObject

import java.util.HashMap


/**
 * ResultData 用于返回值封装，也用于服务端与客户端的 json 数据通信
 * 一、主要应用场景：
 * 1：业务层需要返回多个返回值，例如要返回业务状态以及数据
 *
 * @author ErYang
 */
class ResultData : HashMap<Any, Any>() {

    companion object {

        /**
         * 状态标志
         */
        private const val STATE = "state"

        /**
         * 成功状态
         */
        private const val STATE_OK = "ok"

        /**
         * 失败状态
         */
        private const val STATE_FAIL = "fail"

        /**
         * 重复提交状态
         */
        private const val STATE_REPEAT = "repeat"

        /**
         * 错误状态
         */
        private const val STATE_ERROR = "error"

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建 ResultData
         *
         * @return ResultData
         */
        fun create(): ResultData {
            return ResultData()
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建 ResultData 并赋值
         *
         * @param key   key
         * @param value value
         * @return ResultData
         */
        fun create(key: Any, value: Any): ResultData {
            return ResultData().set(key, value)
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建成功状态的 ResultData
         *
         * @return ResultData
         */
        fun ok(): ResultData {
            return ResultData().setOk()
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建成功状态的 ResultData 并赋值
         *
         * @param key   key
         * @param value value
         * @return ResultData
         */
        fun ok(key: Any, value: Any): ResultData {
            return ok().set(key, value)
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建失败状态的 ResultData
         *
         * @return ResultData
         */
        fun fail(): ResultData {
            return ResultData().setFail()
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建失败状态的 ResultData 并赋值
         *
         * @param key   key
         * @param value value
         * @return ResultData
         */
        fun fail(key: Any, value: Any): ResultData {
            return fail().set(key, value)
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建重复提交状态的 ResultData
         *
         * @return ResultData
         */
        fun repeat(): ResultData {
            return ResultData().setRepeat()
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建重复提交状态的 ResultData 并赋值
         *
         * @param key   key
         * @param value value
         * @return ResultData
         */
        fun repeat(key: Any, value: Any): ResultData {
            return repeat().set(key, value)
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建错误状态的 ResultData
         *
         * @return ResultData
         */
        fun error(): ResultData {
            return ResultData().setError()
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 创建错误状态的 ResultData 并赋值
         *
         * @param key   key
         * @param value value
         * @return ResultData
         */
        fun error(key: Any, value: Any): ResultData {
            return error().set(key, value)
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否是成功状态
     *
     * @return Boolean
     */
    //throw new IllegalStateException("调用 isOk() 之前，必须先调用 ok()、fail() 或者 setOk()、setFail() 方法");
    val isOk: Boolean?
        get() {
            val state: Any? = get(STATE)
            if (STATE_OK == state) {
                return true
            }
            return if (STATE_FAIL == state) {
                false
            } else null
        }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否是失败状态
     *
     * @return Boolean
     */
    //throw new IllegalStateException("调用 isFail() 之前，必须先调用 ok()、fail() 或者 setOk()、setFail() 方法");
    val isFail: Boolean?
        get() {
            val state: Any? = get(STATE)
            if (STATE_FAIL == state) {
                return true
            }
            return if (STATE_OK == state) {
                false
            } else null
        }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置成功状态的 ResultData
     *
     * @return ResultData
     */
    fun setOk(): ResultData {
        put(STATE, STATE_OK)
        return this
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置失败状态的 ResultData
     *
     * @return ResultData
     */
    fun setFail(): ResultData {
        put(STATE, STATE_FAIL)
        return this
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置重复提交状态的 ResultData
     *
     * @return ResultData
     */
    fun setRepeat(): ResultData {
        put(STATE, STATE_REPEAT)
        return this
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置错误状态的 ResultData
     *
     * @return ResultData
     */
    fun setError(): ResultData {
        put(STATE, STATE_ERROR)
        return this
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置 key-value
     *
     * @param key   key
     * @param value value
     * @return ResultData
     */
    operator fun set(key: Any, value: Any): ResultData {
        put(key, value)
        return this
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置map
     *
     * @param map map
     * @return ResultData
     */
    fun set(map: Map<*, *>): ResultData {
        @Suppress("UNCHECKED_CAST")
        super.putAll(map as Map<out Any, Any>)
        return this
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置result
     *
     * @param resultData ResultData
     * @return ResultData
     */
    fun set(resultData: ResultData): ResultData {
        putAll(resultData)
        return this
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除key-value
     *
     * @param key key
     * @return ResultData
     */
    fun delete(key: Any): ResultData {
        super.remove(key)
        return this
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 转为 FastJson
     *
     * @return JSONObject
     */
    fun toFastJson(): JSONObject {
        return JSONObject.toJSON(this) as JSONObject
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false
        return true
    }

    // -------------------------------------------------------------------------------------------------

    override fun hashCode(): Int {
        @Suppress("RemoveExplicitSuperQualifier")
        return super<HashMap>.hashCode()
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ResultData class

/* End of file ResultData.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/util/ResultData.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
