/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.constant


import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

import javax.validation.constraints.NotBlank


/**
 * 七牛配置文件
 *
 *
 * @author ErYang
 */
@Configuration
@PropertySource("classpath:qiniu.properties")
@ConfigurationProperties(prefix = "qiniu")
@EnableCaching
class QiNiuConst {

    /**
     * AK
     */
    @NotBlank
    lateinit var accessKey: String
    /**
     * SK
     */
    @NotBlank
    lateinit var secretKey: String
    /**
     * 七牛机房
     */
    @NotBlank
    lateinit var zone: String
    /**
     * 七牛存储空间名称
     */
    @NotBlank
    lateinit var bucket: String
    /**
     * 七牛存储空间地址
     */
    @NotBlank
    lateinit var baseUrl: String
    /**
     * 上传目录前缀
     */
    @NotBlank
    lateinit var uploadDirPrefix: String

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QiNiuConst class

/* End of file QiNiuConst.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/constant/QiNiuConst.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
