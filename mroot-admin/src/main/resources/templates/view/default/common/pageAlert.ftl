<#-- 页面提示信息模板函数 -->
<#if message??>
<div class="m-alert m-alert--icon m-alert--air alert alert-success fade show" role="alert">
    <div class="m-alert__icon">
        <i class="la la-warning"></i>
    </div>
    <div class="m-alert__text">
        <strong>
            ${message}
        </strong>
    </div>
    <div class="m-alert__close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
    </div>
</div>
</#if>
<#if failMessage??>
<div class="m-alert m-alert--icon m-alert--air alert alert-warning fade show" role="alert">
    <div class="m-alert__icon">
        <i class="la la-warning"></i>
    </div>
    <div class="m-alert__text">
        <strong>${failMessage}</strong>
    </div>
    <div class="m-alert__close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
    </div>
</div>
</#if>
<#if failListMessage??>
<div class="m-alert m-alert--icon m-alert--air alert alert-danger alert-dismissible fade show" role="alert">
    <div class="m-alert__icon">
        <i class="la la-warning"></i>
    </div>
    <div class="m-alert__text">

        <strong>
    <#list failListMessage as item>
        ${item}<br>
    </#list>
        </strong>

    </div>
    <div class="m-alert__close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
    </div>
</div>
</#if>
