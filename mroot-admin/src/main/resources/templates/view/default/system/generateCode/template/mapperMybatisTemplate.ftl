<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${mapperPackageName}.${classPrefix}.${className}Mapper">
    <!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="${modelPackageName}.${classPrefix}.${className}">
    <#list generateModels as var>
        <#if dataId == var.name>
        <#--<id column="${var.name}" property="${var.camelCase}" jdbcType="${var.dataType}"/>-->
          <id column="${var.name}" property="${var.camelCaseName}" jdbcType="${var.dataType}"/>
        <#else>
        <#--<result column="${var.name}" property="${var.camelCase}" jdbcType="${var.dataType}"/>-->
           <result column="${var.name}" property="${var.camelCaseName}" jdbcType="${var.dataType}"/>
        </#if>
    </#list>
    </resultMap>

    <!-- 查询全部列 -->
    <sql id="Base_Column_List">
<#list generateModels as var>
<#if !var_has_next >
    <#if "type" == var.camelCaseName>
        `${var.name}`
    <#elseif "name" == var.camelCaseName>
        `${var.name}`
    <#elseif "title" == var.camelCaseName>
        `${var.name}`
    <#elseif "status" == var.camelCaseName>
        `${var.name}`
    <#elseif "value" == var.camelCaseName>
        `${var.name}`
    <#else>
        ${var.name}
    </#if>
<#else>
    <#if "type" == var.camelCaseName>
        `${var.name}`,
    <#elseif "name" == var.camelCaseName>
        `${var.name}`,
    <#elseif "title" == var.camelCaseName>
        `${var.name}`,
    <#elseif "status" == var.camelCaseName>
        `${var.name}`,
    <#elseif "value" == var.camelCaseName>
        `${var.name}`,
    <#else>
        ${var.name},
    </#if>
</#if>
</#list>
    </sql>

    <!-- 查询列 为了后台分页 -->
    <sql id="Base_Column_List_For_Admin_Page">
<#list generateModels as var>
<#if !var_has_next >
    <#if "type" == var.camelCaseName>
        `${var.name}`
    <#elseif "name" == var.camelCaseName>
        `${var.name}`
    <#elseif "title" == var.camelCaseName>
        `${var.name}`
    <#elseif "status" == var.camelCaseName>
        `${var.name}`
    <#elseif "value" == var.camelCaseName>
        `${var.name}`
    <#else>
        ${var.name}
    </#if>
<#else>
    <#if "type" == var.camelCaseName>
        `${var.name}`,
    <#elseif "name" == var.camelCaseName>
        `${var.name}`,
    <#elseif "title" == var.camelCaseName>
        `${var.name}`,
    <#elseif "value" == var.camelCaseName>
        `${var.name}`,
    <#elseif "status" == var.camelCaseName>
        `${var.name}`,
    <#else>
        ${var.name},
    </#if>
</#if>
</#list>
    </sql>

    <!-- 查询条件 为了分页 -->
    <sql id="Example_Where_Clause_For_Page">
        <#list generateModels as var>
            <#if 0 == var_index>
                <if test="null != ${var.camelCaseName}">
                    AND ${var.name} =  ${r"#{"}${var.camelCaseName}${r"}"}
                </if>
            <#else>
                <#if "username" == var.camelCaseName>
                    <if test="null != ${var.camelCaseName}">
                        AND ${var.name} LIKE CONCAT(${r"#{"}${var.camelCaseName}${r"}"}, '%')
                    </if>
                <#elseif "name" == var.camelCaseName>
                    <if test="null != ${var.camelCaseName}">
                        AND `${var.name}` LIKE CONCAT(${r"#{"}${var.camelCaseName}${r"}"}, '%')
                    </if>
                <#elseif "title" == var.camelCaseName>
                    <if test="null != ${var.camelCaseName}">
                        AND `${var.name}` LIKE CONCAT(${r"#{"}${var.camelCaseName}${r"}"}, '%')
                    </if>
                <#elseif "status" == var.camelCaseName>
                    <if test="null != ${var.camelCaseName}">
                        AND `${var.name}` = ${r"#{"}${var.camelCaseName}${r"}"}
                    </if>
                <#elseif "value" == var.camelCaseName>
                    <if test="null != ${var.camelCaseName}">
                        AND `${var.name}` = ${r"#{"}${var.camelCaseName}${r"}"}
                    </if>
                <#else>
                    <if test="null != ${var.camelCaseName}">
                        AND ${var.name} =  ${r"#{"}${var.camelCaseName}${r"}"}
                    </if>
                </#if>
            </#if>
        </#list>
    </sql>

    <!-- 查询列表为了分页 -->
    <select id="list2Page" resultMap="BaseResultMap">
        SELECT
        <include refid="Base_Column_List_For_Admin_Page"/>
        FROM ${tableName}
        WHERE 1 = 1
        <if test="null != _parameter">
            <include refid="Example_Where_Clause_For_Page"/>
        </if>
    </select>

    <!-- 查询列表 -->
    <select id="listByT" resultMap="BaseResultMap">
        SELECT
        <include refid="Base_Column_List_For_Admin_Page"/>
        FROM ${tableName}
        WHERE 1 = 1
        <if test="null != _parameter">
            <include refid="Example_Where_Clause_For_Page"/>
        </if>
    </select>

    <!-- 得到最大 sort 值 -->
    <select id="getMax2Sort" resultType="java.lang.Integer">
        SELECT MAX(sort) FROM ${tableName}
    </select>

    <!-- 根据 id 批量删除 -->
    <delete id="removeBatchById" parameterType="java.util.List">
        <if test="null != idArray">
            DELETE
            FROM ${tableName}
            WHERE id IN
            <foreach collection="idArray" item="item" open="(" close=")" separator=",">
            ${r"#{"}item${r"}"}
            </foreach>
        </if>
    </delete>

    <!-- 根据 id 批量更新状态 -->
    <update id="updateBatchStatusById" parameterType="java.util.Map">
        <if test="null != idArray">
            UPDATE ${tableName}
            SET `status` = ${r"#{"}status${r"}"},
            gmt_modified = ${r"#{"}gmtModified${r"}"}
            WHERE id IN
            <foreach collection="idArray" item="item" open="(" close=")" separator=",">
            ${r"#{"}item${r"}"}
            </foreach>
        </if>
    </update>


</mapper>
