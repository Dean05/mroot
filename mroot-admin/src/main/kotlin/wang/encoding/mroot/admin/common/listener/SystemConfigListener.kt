/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.listener

import com.alibaba.fastjson.JSON
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import wang.encoding.mroot.admin.common.constant.ConfigConst
import wang.encoding.mroot.common.constant.DatabaseConst
import wang.encoding.mroot.admin.common.constant.Global
import wang.encoding.mroot.admin.common.constant.ResourceConst
import wang.encoding.mroot.admin.common.task.ControllerAsyncTask
import wang.encoding.mroot.admin.common.util.SharedObjectUtil
import wang.encoding.mroot.common.business.Profile
import wang.encoding.mroot.model.entity.system.Config
import wang.encoding.mroot.model.enums.ConfigTypeEnum
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.system.ConfigService
import java.util.*
import javax.servlet.ServletContext
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener


/**
 * 系统监听器
 *
 * @author ErYang
 */
@Configuration
class SystemConfigListener : ServletContextListener {


    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(SystemConfigListener::class.java)
    }

    @Value("\${server.servlet.contextPath}")
    private val contextPath: String? = null

    @Value("\${server.port}")
    private val serverPort: String? = null

    @Autowired
    private lateinit var configProperties: ConfigConst

    @Autowired
    private lateinit var resourceProperties: ResourceConst

    @Autowired
    private lateinit var profile: Profile

    @Autowired
    private lateinit var sharedObjectUtil: SharedObjectUtil

    @Autowired
    private lateinit var controllerAsyncTask: ControllerAsyncTask

    @Autowired
    private lateinit var configService: ConfigService


    @Autowired
    private lateinit var databaseConst: DatabaseConst

    // -------------------------------------------------------------------------------------------------

    /**
     * 服务器启动初始化
     *
     * @param servletContextEvent ServletContextEvent
     */
    override fun contextInitialized(servletContextEvent: ServletContextEvent) {
        if (logger.isInfoEnabled) {
            logger.info(">>>>>>>>ServletContext启动监听器<<<<<<<<")
        }
        val servletContext: ServletContext = servletContextEvent.servletContext

        // 环境
        val activeProfile: String = profile.getActiveProfile()
        if (logger.isInfoEnabled) {
            logger.info(">>>>>>>>设置PROFILE[$activeProfile]<<<<<<<<")
        }
        servletContext.setAttribute(configProperties.profileName, activeProfile)

        if (logger.isInfoEnabled) {
            logger.info(">>>>>>>>CONTEXT_PATH[${contextPath!!}]<<<<<<<<")
        }
        if (null != contextPath && contextPath.isNotBlank()) {
            if (logger.isInfoEnabled) {
                logger.info(">>>>>>>>设置CONTEXT_PATH[$contextPath]<<<<<<<<")
            }
            val webPath: String = contextPath
            servletContext.setAttribute(configProperties.contextPathName, webPath)
        }
        if (null != serverPort && serverPort.isNotBlank()) {
            if (logger.isInfoEnabled) {
                logger.info(">>>>>>>>设置SERVER_PORT[$serverPort]<<<<<<<<")
            }
            servletContext.setAttribute(configProperties.serverPortName, serverPort)
        }
        Global.RESOURCE_MAP = initResourceMap()
        if (logger.isInfoEnabled) {
            logger.info(">>>>>>>>设置资源信息集合[key=${resourceProperties.mapName}," +
                    "map.size=${Global.RESOURCE_MAP.size}]<<<<<<<<")
        }
        servletContext.setAttribute(resourceProperties.mapName, Global.RESOURCE_MAP)
        // 页面使用的全局变量
        sharedObjectUtil.init(servletContext)

        // 系统配置集合
        if (logger.isInfoEnabled) {
            logger.info(">>>>>>>>设置数据库配置信息Global[CONFIG_MAP]<<<<<<<<")
        }
        controllerAsyncTask.reloadConfigMap()

        // 项目启动时 初始化定时器
        controllerAsyncTask.initScheduleJobs()

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 服务器销毁
     *
     * @param servletContextEvent ServletContextEvent
     */
    override fun contextDestroyed(servletContextEvent: ServletContextEvent) {
        if (logger.isInfoEnabled) {
            logger.info(">>>>>>>>ServletContext销毁监听器<<<<<<<<")
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化资源Map 供页面使用
     *
     * @return Map
     */
    private fun initResourceMap(): Map<String, String> {
        // 是否启用七牛cdn
        var qiNiuCdn = false
        // 七牛 cnd 地址
        var qiNiuCdnUrl: String? = null
        // 得到数据库配置的上传配置
        val config: Config? = configService.getByName(databaseConst.qiniuCdnName)
        if (null != config) {
            if (ConfigTypeEnum.FUN.key == config.type) {
                if (null != config.value && config.value!!.isNotBlank()) {
                    // 得到json数据
                    var configMap: Map<*, *>? = null
                    try {
                        // 得到json数据
                        configMap = JSON.parse(config.value) as Map<*, *>
                    } catch (e: Exception) {
                        if (logger.isErrorEnabled) {
                            logger.error(">>>>>>>>得到配置JSON数据异常<<<<<<<<")
                            logger.error(e.message)
                        }
                    }
                    if (null != configMap && configMap.isNotEmpty()) {
                        val status: String? = configMap["status"].toString()
                        if (null != status && StatusEnum.NORMAL.key.toString() == status) {
                            qiNiuCdn = true
                            qiNiuCdnUrl = configMap["url"].toString()
                        }
                    }
                }
            }
        }
        // 设置的 web 路径
        val webPath: String = if (null != contextPath && contextPath.isNotBlank()) {
            contextPath
        } else {
            "/"
        }

        // web 地址
        val webUrl: String = if (qiNiuCdn) {
            // 七牛 CDN
            if (null != qiNiuCdnUrl && qiNiuCdnUrl.isNotBlank()) {
                qiNiuCdnUrl
            } else {
                webPath
            }
        } else {
            webPath
        }

        if (qiNiuCdn) {
            if (logger.isInfoEnabled) {
                logger.info(">>>>>>>>启用七牛CDN[$webUrl]<<<<<<<<")
            }
        }
        val map = HashMap<String, String>(10)
        map[resourceProperties.resourceName] = webUrl + resourceProperties.resource
        map[resourceProperties.vendorsName] = webUrl + resourceProperties.vendors
        map[resourceProperties.defaultName] = webUrl + resourceProperties.default
        map[resourceProperties.appName] = webUrl + resourceProperties.app
        return map
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 检验是否是开发环境
     *
     * @return  Boolean
     */
    private fun devProfile(): Boolean {
        // 开发环境
        return profile.devProfile()
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SystemConfigListener class

/* End of file SystemConfigListener.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/listener/SystemConfigListener.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
