/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.error


import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import wang.encoding.mroot.admin.common.controller.BaseAdminController
import wang.encoding.mroot.common.business.ResultData
import javax.servlet.http.HttpServletRequest


/**
 * 错误控制器
 *
 * @author ErYang
 */
@RestController
class BaseErrorController : ErrorController, BaseAdminController() {


    companion object {

        /**
         * 错误页面地址
         */
        private const val ERROR_URL: String = "/error"

        /**
         * 404错误页面地址
         */
        private const val ERROR404_URL: String = "/error/404"

        /**
         * 默认视图目录
         */
        private const val DEFAULT_VIEW: String = "/default"

        /**
         * 错误视图
         */
        private const val ERROR_EXCEPTION_VIEW: String = "$DEFAULT_VIEW/error/404"


        /**
         * 无权限页面地址
         */
        private const val ERROR403_URL: String = "/error/403"

        /**
         * 无权限页面试图
         */
        private const val ERROR403_EXCEPTION_VIEW: String = "$DEFAULT_VIEW/error/403"

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 错误页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = [ERROR404_URL])
    fun error404(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(ERROR_EXCEPTION_VIEW)
        val url: String? = request.getHeader("Referer")
        if (null != url) {
            modelAndView.addObject("refererUrl", url)
        }
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 错误页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = [ERROR_URL])
    fun handleError404(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(ERROR_EXCEPTION_VIEW)
        val url: String? = request.getHeader("Referer")
        if (null != url) {
            modelAndView.addObject("refererUrl", url)
        }
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 无权限错误页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = [ERROR403_URL])
    fun handleError403(request: HttpServletRequest): Any {
        return if (httpRequestUtil.isAjaxRequest(request)) {
            val result: ResultData = ResultData.error()
            result.setError()[configProperties.messageName] = localeMessageSourceConfiguration
                    .getMessage("message.exception403.content")
            result.toFastJson()
        } else {
            val modelAndView = ModelAndView(ERROR403_EXCEPTION_VIEW)
            val url: String? = request.getHeader("Referer")
            if (null != url) {
                modelAndView.addObject("refererUrl", url)
            }
            modelAndView
        }
    }

    // -------------------------------------------------------------------------------------------------

    override fun getErrorPath(): String {
        return ERROR_EXCEPTION_VIEW
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseErrorController class

/* End of file BaseErrorController.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/controller/error/BaseErrorController.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
