/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.config


import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import wang.encoding.mroot.admin.common.interceptor.BaseInterceptor
import wang.encoding.mroot.common.interceptor.FormTokenInterceptor
import wang.encoding.mroot.common.interceptor.SqlInjectInterceptor


/**
 * 拦截器配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
class WebAppConfigurer : WebMvcConfigurer {


    /**
     * 初始化 BaseInterceptor
     *
     * @return BaseInterceptor
     */
    val baseInterceptor: BaseInterceptor
        @Bean
        get() = BaseInterceptor()

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 TokenInterceptor
     *
     * @return TokenInterceptor
     */
    val formTokenInterceptor: FormTokenInterceptor
        @Bean
        get() = FormTokenInterceptor()

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 SqlInjectInterceptor
     *
     * @return SqlInjectInterceptor
     */
    val sqlInjectInterceptor: SqlInjectInterceptor
        @Bean
        get() = SqlInjectInterceptor()

    // -------------------------------------------------------------------------------------------------

    /**
     * 拦截器
     *
     * @param registry InterceptorRegistry
     */
    override fun addInterceptors(registry: InterceptorRegistry) {


        registry.addInterceptor(baseInterceptor).addPathPatterns("/**")
                .excludePathPatterns("/", "/assets/**", "/verifyImage", "/login",
                        "/doLogin", "/**/save**", "/**/update**",
                        "/**/delete**", "/**/**Save**/", "/**recover**")

        // SqlInjectInterceptor 防止 sql 注入
        registry.addInterceptor(sqlInjectInterceptor).addPathPatterns("/**")
                .excludePathPatterns("/**/favicon.ico", "/assets/**",
                        "/verifyImage")

        /* 表单重复提交 */
        // 登录 用户
        registry.addInterceptor(formTokenInterceptor)
                .addPathPatterns("/", "/login", "/doLogin",
                        "/user/**",
                        "/role/**",
                        "/rule/**",
                        "/config/**",
                        "/category/**",
                        "/article/**",
                        "/scheduleJob/**"
                )
                .excludePathPatterns("/assets/**", "/verifyImage",
                        "/user/index", "/user/view", "/user/recycleBin",
                        "/role/index", "/role/view", "/role/recycleBin",
                        "/rule/index", "/rule/view", "/rule/recycleBin",
                        "/config/index", "/config/view", "/config/recycleBin",
                        "/category/index", "/category/view", "/category/recycleBin",
                        "/article/index", "/article/view", "/article/recycleBin",
                        "/scheduleJob/index", "/scheduleJob/view"
                )


    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End WebAppConfigurer class

/* End of file WebAppConfigurer.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/config/WebAppConfigurer.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
