/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.admin.common.task


import com.alibaba.fastjson.JSON
import org.apache.shiro.session.Session
import org.quartz.CronExpression
import org.quartz.CronTrigger
import org.quartz.Scheduler
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Async
import org.springframework.scheduling.annotation.AsyncResult
import org.springframework.stereotype.Component
import wang.encoding.mroot.admin.common.constant.ConfigConst
import wang.encoding.mroot.common.constant.DatabaseConst
import wang.encoding.mroot.admin.common.constant.Global
import wang.encoding.mroot.admin.common.constant.ResourceConst
import wang.encoding.mroot.admin.common.quartz.QuartzScheduleUtil
import wang.encoding.mroot.model.entity.system.*
import wang.encoding.mroot.model.enums.ConfigTypeEnum
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.system.*
import java.math.BigInteger
import java.util.*
import java.util.concurrent.Future
import javax.cache.Cache
import javax.servlet.http.HttpServletRequest
import kotlin.collections.ArrayList


/**
 * 异步调用
 *
 * @author ErYang
 */
@Component
class ControllerAsyncTask {

    @Value("\${server.servlet.contextPath}")
    private val contextPath: String? = null

    @Autowired
    protected lateinit var configProperties: ConfigConst

    @Autowired
    protected lateinit var requestLogService: RequestLogService

    @Autowired
    protected lateinit var configService: ConfigService

    @Autowired
    protected lateinit var userService: UserService

    @Autowired
    private lateinit var ruleService: RuleService

    @Autowired
    private lateinit var roleService: RoleService

    @Autowired
    private lateinit var scheduleJobService: ScheduleJobService

    @Autowired
    private lateinit var scheduler: Scheduler

    @Autowired
    private lateinit var configCache: Cache<Any, Any>

    @Autowired
    private lateinit var resourceProperties: ResourceConst

    @Autowired
    private lateinit var databaseConst: DatabaseConst

    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(ControllerAsyncTask::class.java)
    }

    /**
     * 设置 session 中登录用户信息
     *
     * @param user User
     *
     * @return Future<String>
     */
    @Async("adminTaskExecutePool")
    fun initCurrentAdmin(user: User, session: Session): Future<String> {
        session.setAttribute(configProperties.adminSessionName,
                userService.aseDecryptData(user))
        return AsyncResult(">>>>>>>>initCurrentAdmin执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 重新设置 当前用户角色拥有的权限
     *
     * @param user User
     * @param session Session
     *
     * @return Future<String>
     */
    @Async("adminTaskExecutePool")
    fun initUserRoleHasRules(user: User, session: Session): Future<String> {
        // 权限
        val rules: MutableList<Rule> = mutableListOf()
        // 所属角色
        val roles: Set<Role> = roleService.listByUserId(user.id!!)!!
        if (roles.isNotEmpty()) {
            for (role: Role in roles) {
                val ruleList: TreeSet<Rule>? = ruleService.listByRoleId(role.id!!)
                if (null !== ruleList && ruleList.isNotEmpty()) {
                    for (rule: Rule in ruleList) {
                        if (BigInteger.ZERO != rule.pid) {
                            rules.add(rule)
                        }
                    }
                }
            }
            if (rules.isNotEmpty()) {
                // list 转为 tree
                val tree: List<Rule>? = Rule.list2Tree(rules)
                if (null != tree) {
                    // 用户权限菜单存放在 session 中
                    session.setAttribute(configProperties.adminMenuName, tree)
                }
            }
        }
        return AsyncResult(">>>>>>>>initUserRoleHasRules执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清空请求日志
     *
     * @return Future<String>
     */
    @Async("adminTaskExecutePool")
    fun truncateRequestLog(): Future<String> {
        requestLogService.truncate()
        return AsyncResult(">>>>>>>>truncateRequestLog执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 项目启动时 初始化定时任务
     *
     * @return Future<String>
     */
    @Async("adminTaskExecutePool")
    fun initScheduleJobs(): Future<String> {
        if (logger.isErrorEnabled) {
            logger.info(">>>>>>>>初始化定时任务<<<<<<<<")
        }
        val whereMap: Map<String, Any> = mapOf(ScheduleJob.STATUS to StatusEnum.NORMAL.key)
        val scheduleJobList: List<ScheduleJob>? = scheduleJobService.list(whereMap, null)
        scheduleJobList?.stream()?.filter { scheduleJob ->
            CronExpression.isValidExpression(scheduleJob.cronExpression)
                    && StatusEnum.NORMAL.key == scheduleJob.status
        }?.forEach { scheduleJob ->
            var cronTrigger: CronTrigger? = null
            try {
                cronTrigger = QuartzScheduleUtil.getCronTrigger(scheduler)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // 如果不存在，则创建
            if (null == cronTrigger) {
                try {
                    QuartzScheduleUtil.deleteScheduleJob(scheduler, scheduleJob)
                    QuartzScheduleUtil.addScheduleJob(scheduler, scheduleJob)
                } catch (e: Exception) {
                    if (logger.isErrorEnabled) {
                        logger.info(">>>>>>>>创建定时任务失败$e<<<<<<<<")
                    }
                }
            } else {
                try {
                    QuartzScheduleUtil.editScheduleJob(scheduler, scheduleJob)
                } catch (e: Exception) {
                    e.printStackTrace()
                    if (logger.isErrorEnabled) {
                        logger.info(">>>>>>>>更新定时任务失败$e<<<<<<<<")
                    }
                }
            }
        }
        return AsyncResult(">>>>>>>>initScheduleJobs执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 重载系统配置信息
     *
     * @return Future<String>
     */
    @Async("adminTaskExecutePool")
    fun reloadConfigMap(): Future<String> {
        val configMap: MutableMap<String, Config> = mutableMapOf()
        val configWhereMap: Map<String, Any> = mapOf(Config.STATUS to StatusEnum.NORMAL.key)
        val configList: List<Config>? = configService.list(configWhereMap, null)
        if (null != configList && configList.isNotEmpty()) {
            for (i: Int in configList.indices) {
                val config: Config = configList[i]
                configMap[config.name!!] = config
            }
            Global.CONFIG_MAP = configMap
        }
        return AsyncResult(">>>>>>>>reloadConfigMap执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新系统配置时 静态文件配置
     *
     * @return Future<String>
     */
    @Async("adminTaskExecutePool")
    fun reloadResourceMap(request: HttpServletRequest, config: Config): Future<String> {
        // 是否启用七牛cdn
        var qiNiuCdn = false
        // 七牛 cnd 地址
        var qiNiuCdnUrl: String? = null
        if (databaseConst.qiniuCdnName == config.name) {
            if (ConfigTypeEnum.FUN.key == config.type) {
                if (null != config.value && config.value!!.isNotBlank()) {
                    // 得到json数据
                    var configMap: Map<*, *>? = null
                    try {
                        // 得到json数据
                        configMap = JSON.parse(config.value) as Map<*, *>
                    } catch (e: Exception) {
                        if (logger.isErrorEnabled) {
                            logger.error(">>>>>>>>得到配置JSON数据异常<<<<<<<<")
                            logger.error(e.message)
                        }
                    }
                    if (null != configMap && configMap.isNotEmpty()) {
                        val status: String? = configMap["status"].toString()
                        if (null != status && StatusEnum.NORMAL.key.toString() == status) {
                            qiNiuCdn = true
                            qiNiuCdnUrl = configMap["url"].toString()
                        }
                    }
                }
            }
        }
        // 设置的 web 路径
        val webPath: String = if (null != contextPath && contextPath.isNotBlank()) {
            contextPath
        } else {
            "/"
        }

        // web 地址
        val webUrl: String = if (qiNiuCdn) {
            // 七牛 CDN
            if (null != qiNiuCdnUrl && qiNiuCdnUrl.isNotBlank()) {
                qiNiuCdnUrl
            } else {
                webPath
            }
        } else {
            webPath
        }

        if (qiNiuCdn) {
            if (logger.isInfoEnabled) {
                logger.info(">>>>>>>>启用七牛CDN[$webUrl]<<<<<<<<")
            }
        }
        val map = HashMap<String, String>(10)
        map[resourceProperties.resourceName] = webUrl + resourceProperties.resource
        map[resourceProperties.vendorsName] = webUrl + resourceProperties.vendors
        map[resourceProperties.defaultName] = webUrl + resourceProperties.default
        map[resourceProperties.appName] = webUrl + resourceProperties.app
        Global.RESOURCE_MAP = map
        request.session.servletContext.setAttribute(resourceProperties.mapName, Global.RESOURCE_MAP)
        if (logger.isInfoEnabled) {
            logger.info(">>>>>>>>重新设置资源信息集合[key=${resourceProperties.mapName}," +
                    "map.size=${Global.RESOURCE_MAP.size}]<<<<<<<<")
        }
        return AsyncResult(">>>>>>>>reloadResourceMap执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 删除 角色-权限
     *
     * @param roleId BigInteger
     *
     * @return Future<String>
     */
    @Async("adminTaskExecutePool")
    fun removeByRoleId(roleId: BigInteger): Future<String> {
        ruleService.removeByRoleId(roleId)
        return AsyncResult(">>>>>>>>removeByRoleId执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 删除 角色-权限
     *
     * @param roleIdArray Array<BigInteger>
     * @return Int
     */
    @Async("adminTaskExecutePool")
    fun removeByRoleIdArray(roleIdArray: ArrayList<BigInteger>?): Future<String> {
        ruleService.removeByRoleIdArray(roleIdArray)
        return AsyncResult(">>>>>>>>removeByRoleIdArray执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户id 删除 用户-角色
     *
     * @param userId BigInteger
     *
     * @return Future<String>
     */
    @Async("adminTaskExecutePool")
    fun removeByUserId(userId: BigInteger): Future<String> {
        roleService.removeByUserId(userId)
        return AsyncResult(">>>>>>>>removeByUserId执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户id 删除 用户-角色
     *
     * @param userIdArray Array<BigInteger>
     * @return Int
     */
    @Async("adminTaskExecutePool")
    fun removeByUserIdArray(userIdArray: ArrayList<BigInteger>?): Future<String> {
        roleService.removeByUserIdArray(userIdArray)
        return AsyncResult(">>>>>>>>removeByUserIdArray执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清空所有缓存
     *
     * @return Int
     */
    @Async("adminTaskExecutePool")
    fun clearAllCache(): Future<String> {
        configCache.removeAll()
        return AsyncResult(">>>>>>>>clearAllCache执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ControllerAsyncTask class

/* End of file ControllerAsyncTask.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/task/ControllerAsyncTask.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------
