/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.config


import com.alibaba.fastjson.JSONObject
import com.baomidou.mybatisplus.plugins.Page
import org.apache.shiro.authz.annotation.RequiresPermissions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import wang.encoding.mroot.admin.common.controller.BaseAdminController
import wang.encoding.mroot.common.annotation.FormToken
import wang.encoding.mroot.common.annotation.RequestLogAnnotation
import wang.encoding.mroot.common.business.ResultData
import wang.encoding.mroot.common.constant.RequestLogConstant
import wang.encoding.mroot.common.exception.ControllerException
import wang.encoding.mroot.model.entity.system.Config
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.event.RemoveConfigCacheEvent
import wang.encoding.mroot.service.system.ConfigService
import java.math.BigInteger
import java.util.*
import javax.servlet.http.HttpServletRequest


/**
 * 后台 系统配置 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping(value = ["/config"])
class ConfigController : BaseAdminController() {


    @Autowired
    private lateinit var configService: ConfigService


    companion object {

        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(ConfigController::class.java)

        /**
         * 模块
         */
        private const val MODULE_NAME: String = "/config"

        /**
         * 视图目录
         */
        private const val VIEW_PATH: String = "/system/config"

        /**
         * 对象名称
         */
        private const val VIEW_MODEL_NAME: String = "config"
        /**
         * 首页
         */
        private const val INDEX: String = "/index"
        private const val INDEX_URL: String = MODULE_NAME + INDEX
        private const val INDEX_VIEW: String = VIEW_PATH + INDEX
        /**
         * 新增
         */
        private const val ADD: String = "/add"
        private const val ADD_URL: String = MODULE_NAME + ADD
        private const val ADD_VIEW: String = VIEW_PATH + ADD
        /**
         * 保存
         */
        private const val SAVE: String = "/save"
        private const val SAVE_URL: String = MODULE_NAME + SAVE
        /**
         * 修改
         */
        private const val EDIT: String = "/edit"
        private const val EDIT_URL: String = MODULE_NAME + EDIT
        private const val EDIT_VIEW: String = VIEW_PATH + EDIT
        /**
         * 更新
         */
        private const val UPDATE: String = "/update"
        private const val UPDATE_URL: String = MODULE_NAME + UPDATE
        /**
         * 查看
         */
        private const val VIEW: String = "/view"
        private const val VIEW_URL: String = MODULE_NAME + VIEW
        private const val VIEW_VIEW: String = VIEW_PATH + VIEW
        /**
         * 删除
         */
        private const val DELETE: String = "/delete"
        private const val DELETE_URL: String = MODULE_NAME + DELETE
        private const val DELETE_BATCH: String = "/deleteBatch"
        private const val DELETE_BATCH_URL: String = MODULE_NAME + DELETE_BATCH

        /**
         * 回收站
         */
        private const val RECYCLE_BIN_INDEX: String = "/recycleBin"
        private const val RECYCLE_BIN_INDEX_URL: String = MODULE_NAME + RECYCLE_BIN_INDEX
        private const val RECYCLE_BIN_INDEX_VIEW: String = VIEW_PATH + RECYCLE_BIN_INDEX
        /**
         * 恢复
         */
        private const val RECOVER: String = "/recover"
        private const val RECOVER_URL: String = MODULE_NAME + RECOVER

        private const val RECOVER_BATCH: String = "/recoverBatch"
        private const val RECOVER_BATCH_URL: String = MODULE_NAME + RECOVER_BATCH


        /**
         * 清空缓存
         */
        private const val CLEAR_CACHE: String = "/clearCache"
        private const val CLEAR_CACHE_URL: String = MODULE_NAME + CLEAR_CACHE

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [INDEX_URL])
    @RequestMapping(INDEX)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_INDEX)
    @Throws(ControllerException::class)
    fun index(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(INDEX_VIEW))
        super.initViewTitleAndModelUrl(INDEX_URL, MODULE_NAME, request)

        val config = Config()
        val title: String? = request.getParameter("title")
        if (null != title && title.isNotBlank()) {
            config.title = title
            modelAndView.addObject("title", title)
        }
        config.status = StatusEnum.NORMAL.key
        val page: Page<Config> = configService.list2page(super.initPage(request), config, Config.ID, false)!!
        modelAndView.addObject(VIEW_PAGE_NAME, page)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [ADD_URL])
    @RequestMapping(ADD)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_ADD)
    @FormToken(init = true)
    @Throws(ControllerException::class)
    fun add(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(ADD_VIEW))
        super.initViewTitleAndModelUrl(ADD_URL, MODULE_NAME, request)

        // 最大排序值
        val maxSort: Int = configService.getMax2Sort() + 1
        modelAndView.addObject("maxSort", maxSort)

        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param request HttpServletRequest
     * @param config Config
     * @return ModelAndView
     */
    @RequiresPermissions(value = [SAVE_URL])
    @RequestMapping(SAVE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_SAVE)
    @FormToken(remove = true)
    @Throws(ControllerException::class)
    fun save(request: HttpServletRequest, config: Config): Any {
        if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()

            // 创建 Config 对象
            val saveConfig: Config = this.initAddData(request, config)

            // Hibernate Validation  验证数据
            val validationResult: String? = configService.validationConfig(saveConfig)
            if (null != validationResult && validationResult.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }

            // 验证数据唯一性
            val flag: Boolean = this.validationAddData(saveConfig, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }

            // 新增用户 id
            val id: BigInteger? = configService.saveBackId(saveConfig)
            if (null != id && BigInteger.ZERO < id) {
                // 重载系统配置
                controllerAsyncTask.reloadConfigMap()
                // 异步清理缓存
                this.removeCacheEvent(id)
            }
            return super.initSaveJSONObject(id)
        } else {
            return super.initErrorRedirectUrl()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(value = [EDIT_URL])
    @RequestMapping("$EDIT/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_EDIT)
    @FormToken(init = true)
    @Throws(ControllerException::class)
    fun edit(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(EDIT_VIEW))


        super.initViewTitleAndModelUrl(EDIT_URL, MODULE_NAME, request)
        val idValue: BigInteger? = super.getId(id)
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        // 数据真实性
        val config: Config? = configService.getById2Cache(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (StatusEnum.DELETE.key == config!!.status) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        modelAndView.addObject(VIEW_MODEL_NAME, config)
        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @param request HttpServletRequest
     * @param config Config
     * @return ModelAndView
     */
    @RequiresPermissions(value = [UPDATE_URL])
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_UPDATE)
    @FormToken(remove = true)
    @Throws(ControllerException::class)
    fun update(request: HttpServletRequest, config: Config): Any {
        if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()
            // 验证数据
            val idValue: BigInteger? = super.getId(request)
            if (null == idValue || BigInteger.ZERO > idValue) {
                return super.initErrorCheckJSONObject(failResult)
            }
            // 数据真实性
            val configBefore: Config = configService.getById2Cache(idValue)
                    ?: return super.initErrorCheckJSONObject(failResult)
            if (StatusEnum.DELETE.key == configBefore.status) {
                return super.initErrorCheckJSONObject(failResult)
            }


            // 创建 Config 对象
            var editConfig: Config = Config.copy2New(config)
            editConfig.id = configBefore.id
            val statusStr: String? = super.getStatusStr(request)
            when {
                statusStr.equals(configProperties.bootstrapSwitchEnabled) -> editConfig.status = StatusEnum.NORMAL.key
                null == statusStr -> editConfig.status = StatusEnum.DISABLE.key
                else -> editConfig.status = StatusEnum.DISABLE.key
            }
            editConfig = this.initEditData(editConfig)

            // Hibernate Validation 验证数据
            val validationResult: String? = configService.validationConfig(editConfig)
            if (null != validationResult && validationResult.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }


            // 验证数据唯一性
            val flag: Boolean = this.validationEditData(config, configBefore, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }

            // 修改用户 id
            val id: BigInteger? = configService.updateBackId(editConfig)
            if (null != id && BigInteger.ZERO < id) {
                // 重载系统配置
                controllerAsyncTask.reloadConfigMap()
                // 静态文件配置
                controllerAsyncTask.reloadResourceMap(request, editConfig)
                // 异步清理缓存
                this.removeCacheEvent(id)
            }
            return super.initUpdateJSONObject(id)
        } else {
            return super.initErrorRedirectUrl()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(value = [VIEW_URL])
    @RequestMapping("$VIEW/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_VIEW)
    @Throws(ControllerException::class)
    fun view(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(VIEW_VIEW))
        super.initViewTitleAndModelUrl(VIEW_URL, MODULE_NAME, request)

        val idValue: BigInteger? = super.getId(id)
        // 验证数据
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        // 数据真实性
        val config: Config? = configService.getById2Cache(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (null != config) {
            modelAndView.addObject(VIEW_MODEL_NAME, config)
        }
        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [DELETE_URL])
    @RequestMapping("$DELETE/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_DELETE)
    @Throws(ControllerException::class)
    fun delete(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val config: Config? = configService.getById2Cache(idValue!!)
            if (null == config) {
                super.initErrorCheckJSONObject()
            }
            // 删除 Config id
            val backId: BigInteger? = configService.removeBackId(config!!.id!!)
            if (null != backId && BigInteger.ZERO < backId) {
                // 重载系统配置
                controllerAsyncTask.reloadConfigMap()
                // 异步清理缓存
                this.removeCacheEvent(backId)
            }
            return super.initDeleteJSONObject(backId)
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     * @param request HttpServletRequest
     * @return JSONObject
     */
    @RequiresPermissions(value = [DELETE_BATCH_URL])
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_DELETE_BATCH)
    @Throws(ControllerException::class)
    fun deleteBatch(request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idArray: ArrayList<BigInteger>? = super.getIdArray(request)
            // 验证数据
            if (null == idArray || idArray.isEmpty()) {
                super.initErrorCheckJSONObject()
            }
            var flag = false
            if (null != idArray) {
                flag = configService.removeBatch2UpdateStatus(idArray)!!
            }
            return if (flag) {
                // 重载系统配置
                controllerAsyncTask.reloadConfigMap()
                // 异步清理缓存
                this.removeBatchCacheEvent(idArray!!)
                super.initDeleteJSONObject(BigInteger.ONE)
            } else {
                super.initDeleteJSONObject(BigInteger.ZERO)
            }
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 回收站页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [RECYCLE_BIN_INDEX_URL])
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_RECYCLE_BIN_INDEX)
    @Throws(ControllerException::class)
    fun recycleBin(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(RECYCLE_BIN_INDEX_VIEW))
        super.initViewTitleAndModelUrl(RECYCLE_BIN_INDEX_URL, MODULE_NAME, request)

        val config = Config()
        val title: String? = request.getParameter("title")
        if (null != title && title.isNotBlank()) {
            config.title = title
            modelAndView.addObject("title", title)
        }
        config.status = StatusEnum.DELETE.key
        val page: Page<Config> = configService.list2page(super.initPage(request), config, Config.ID, false)!!
        modelAndView.addObject(VIEW_PAGE_NAME, page)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [RECOVER_URL])
    @RequestMapping("$RECOVER/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_RECOVER)
    @Throws(ControllerException::class)
    fun recover(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val config: Config? = configService.getById2Cache(idValue!!)
            if (null == config) {
                super.initErrorCheckJSONObject()
            }
            // 恢复 id
            val backId: BigInteger? = configService.recoverBackId(config!!.id!!)
            if (null != backId && BigInteger.ZERO < backId) {
                // 重载系统配置
                controllerAsyncTask.reloadConfigMap()
                // 异步清理缓存
                this.removeCacheEvent(backId)
            }
            return super.initRecoverJSONObject(backId)
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量恢复
     * @param request HttpServletRequest
     * @return JSONObject
     */
    @RequiresPermissions(value = [RECOVER_BATCH_URL])
    @RequestMapping(RECOVER_BATCH)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_RECOVER_BATCH)
    @Throws(ControllerException::class)
    fun recoverBatch(request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idArray: ArrayList<BigInteger>? = super.getIdArray(request)
            // 验证数据
            if (null == idArray || idArray.isEmpty()) {
                super.initErrorCheckJSONObject()
            }
            var flag = false
            if (null != idArray) {
                flag = configService.recoverBatch2UpdateStatus(idArray)!!
            }
            return if (flag) {
                // 重载系统配置
                controllerAsyncTask.reloadConfigMap()
                // 异步清理缓存
                this.removeBatchCacheEvent(idArray!!)
                super.initRecoverJSONObject(BigInteger.ONE)
            } else {
                super.initRecoverJSONObject(BigInteger.ZERO)
            }
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清空缓存
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [CLEAR_CACHE_URL])
    @RequestMapping(CLEAR_CACHE)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CONFIG_CLEAR_CACHE)
    @Throws(ControllerException::class)
    fun clearCache(request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            // 异步清空缓存
            controllerAsyncTask.clearAllCache()
            return super.initTruncateJSONObject(BigInteger.ONE)
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param request HttpServletRequest
     * @param config         Config
     * @return Config
     */
    private fun initAddData(request: HttpServletRequest, config: Config): Config {
        val addConfig: Config = Config.copy2New(config)
        // ip
        addConfig.gmtCreateIp = super.getIp(request)
        // 创建 Config 对象
        return configService.initSaveConfig(addConfig)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param config         Config
     * @return Config
     */
    private fun initEditData(config: Config): Config {
        val editConfig: Config = config
        // 创建 Config 对象
        return configService.initEditConfig(editConfig)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param config         Config
     * @param failResult ResultData
     * @return Boolean true(通过)/false(未通过)
     */
    private fun validationAddData(config: Config, failResult: ResultData): Boolean {
        val message: String
        // 是否存在
        val nameExist: Config? = configService.getByName(config.name!!.trim().toLowerCase())
        if (null != nameExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_NAME_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }
        // 是否存在
        val titleExist: Config? = configService.getByTitle(config.title!!.trim().toLowerCase())
        if (null != titleExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_TITLE_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }
        return true
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param config         Config
     * @param failResult ResultData
     * @return Boolean true(通过)/false(未通过)
     */
    private fun validationEditData(newConfig: Config, config: Config,
                                   failResult: ResultData): Boolean {
        val message: String
        // 是否存在
        val nameExist: Boolean = configService.propertyUnique(Config.NAME,
                newConfig.name!!.trim().toLowerCase(), config.name!!.toLowerCase())
        if (!nameExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_NAME_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }

        val titleExist: Boolean = configService.propertyUnique(Config.TITLE,
                newConfig.title!!.trim().toLowerCase(), config.title!!.toLowerCase())
        if (!titleExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_TITLE_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }
        return true
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 ID 清理缓存
     *
     * @param id         BigInteger
     *
     */
    private fun removeCacheEvent(id: BigInteger) {
        // 异步清理缓存
        val removeConfigCacheEvent = RemoveConfigCacheEvent(this, id)
        super.applicationContext.publishEvent(removeConfigCacheEvent)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 ID 集合批量清理缓存
     *
     * @param idArray         ArrayList<BigInteger>
     *
     */
    private fun removeBatchCacheEvent(idArray: ArrayList<BigInteger>) {
        // 异步清理缓存
        val removeConfigCacheEvent = RemoveConfigCacheEvent(this, idArray)
        super.applicationContext.publishEvent(removeConfigCacheEvent)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigController class

/* End of file ConfigController.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/controller/system/config/ConfigController.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
