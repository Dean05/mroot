/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.constant


import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

import javax.validation.constraints.NotBlank


/**
 * 系统配置文件
 * 用户页面变量名称使用
 *
 * @author ErYang
 */
@Configuration
@PropertySource("classpath:config.properties")
@ConfigurationProperties(prefix = "config")
@EnableCaching
class ConfigConst {

    /**
     * 模式变量名称
     */
    @NotBlank
    lateinit var profileName: String

    /**
     * 地址变量名称
     */
    @NotBlank
    lateinit var contextPathName: String

    /**
     * 端口号变量名称
     */
    @NotBlank
    lateinit var serverPortName: String

    /**
     * 后台登录地址变量名称
     */
    @NotBlank
    lateinit var adminLoginUrlName: String

    /**
     * 处理后台登录地址变量名称
     */
    @NotBlank
    lateinit var adminDoLoginUrlName: String

    /**
     * 后台退出地址变量名称
     */
    @NotBlank
    lateinit var adminLogoutUrlName: String

    /**
     * 改变语言
     */
    @NotBlank
    lateinit var adminLanguageUrlName: String

    /**
     * 后台未授权地址变量名称
     */
    @NotBlank
    lateinit var adminUnauthorizedUrlName: String

    /**
     * 后台静态文件地址变量名称
     */
    @NotBlank
    lateinit var adminStaticPathUrlName: String

    /**
     * 后台验证码地址变量名称
     */
    @NotBlank
    lateinit var adminVerifyImageUrlName: String

    /**
     * 后台登录用户 session 名称
     */
    @NotBlank
    lateinit var adminSessionName: String

    /**
     * 后台登录用户 sessionId 名称
     */
    @NotBlank
    lateinit var adminSessionIdName: String

    /**
     * 后台管理员权限菜单
     */
    @NotBlank
    lateinit var adminMenuName: String

    /**
     * i18n变量名称
     */
    @NotBlank
    lateinit var i18nName: String

    /**
     * shiro 标签变量名称
     */
    @NotBlank
    lateinit var shiroName: String

    /**
     * 权限变量名称
     */
    @NotBlank
    lateinit var ruleName: String

    /**
     * 文章变量名称
     */
    @NotBlank
    lateinit var categoryName: String

    /**
     * freemarker 共享对象变量名称
     */
    @NotBlank
    lateinit var freemarkerSharedVariableBlockName: String
    @NotBlank
    lateinit var freemarkerSharedVariableOverrideName: String
    @NotBlank
    lateinit var freemarkerSharedVariableExtendsName: String

    /**
     * 后台分页显示条数
     */
    @NotBlank
    lateinit var adminPageSize: String

    /**
     * 提示信息字段变量名称
     */
    @NotBlank
    lateinit var messageName: String
    /**
     * 验证提示信息字段变量名称
     */
    @NotBlank
    lateinit var validationMessageName: String
    /**
     * 成功提示信息字段变量名称
     */
    @NotBlank
    lateinit var successMessageName: String
    /**
     * 失败提示信息字段变量名称
     */
    @NotBlank
    lateinit var failMessageName: String
    /**
     * 失败提示信息字段集合变量名称
     */
    @NotBlank
    lateinit var failMessageListName: String
    /**
     * bootstrap-switch 插件的启用状态标识
     */
    @NotBlank
    lateinit var bootstrapSwitchEnabled: String

    /**
     * 用户类型 map 集合名称
     */
    @NotBlank
    lateinit var userTypeMap: String

    /**
     * 权限类型 map 集合名称
     */
    @NotBlank
    lateinit var ruleTypeMap: String
    /**
     * 权限类型 根地址
     */
    @NotBlank
    lateinit var ruleTypeRoot: String
    /**
     * 权限类型 URL地址
     */
    @NotBlank
    lateinit var ruleTypeUrl: String
    /**
     * 权限类型   菜单
     */
    @NotBlank
    lateinit var ruleTypeMenu: String
    /**
     * 权限类型 子级菜单
     */
    @NotBlank
    lateinit var ruleTypeChildMenu: String
    /**
     * 权限类型 按钮
     */
    @NotBlank
    lateinit var ruleTypeButton: String

    /**
     * 状态 map 集合名称
     */
    @NotBlank
    lateinit var statusMap: String
    /**
     * 状态 正常
     */
    @NotBlank
    lateinit var statusNormal: String
    /**
     * 状态 禁用
     */
    @NotBlank
    lateinit var statusDisable: String
    /**
     * 状态   已删除
     */
    @NotBlank
    lateinit var statusDelete: String
    /**
     * 默认密码
     */
    @NotBlank
    lateinit var defaultPassword: String

    /**
     * 系统配置类型 map 集合名称
     */
    @NotBlank
    lateinit var configTypeMap: String
    /**
     * 系统配置类型 值
     */
    @NotBlank
    lateinit var configTypeValue: String
    /**
     * 系统配置类型 表达式
     */
    @NotBlank
    lateinit var configTypeFun: String

    /**
     * 是/否 map 集合名称
     */
    @NotBlank
    lateinit var booleMap: String
    /**
     * 是/否 是
     */
    @NotBlank
    lateinit var booleYes: String
    /**
     * 是/否 否
     */
    @NotBlank
    lateinit var booleNo: String

    // -------------------------------------------------------------------------------------------------

    /**
     * 文章分类类型 map 集合名称
     */
    @NotBlank
    lateinit var categoryTypeMap: String
    /**
     * 文章分类类型 主分类
     */
    @NotBlank
    lateinit var categoryTypeFirst: String
    /**
     * 文章分类类型 次级分类
     */
    @NotBlank
    lateinit var categoryTypeSecond: String
    /**
     * 文章分类类型 分类
     */
    @NotBlank
    lateinit var categoryTypeThird: String

    // -------------------------------------------------------------------------------------------------

    /**
     * 国际化语言名称
     */
    @NotBlank
    lateinit var i18nLanguageName: String

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigConst class

/* End of file ConfigConst.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/constant/ConfigConst.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
