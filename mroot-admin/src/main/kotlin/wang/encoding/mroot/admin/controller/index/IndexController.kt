/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.index


import org.apache.shiro.authz.annotation.RequiresPermissions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import wang.encoding.mroot.admin.common.controller.BaseAdminController
import wang.encoding.mroot.admin.common.util.ComputerHardwareUtil
import wang.encoding.mroot.admin.common.util.ShiroSessionUtil
import wang.encoding.mroot.common.annotation.FormToken
import wang.encoding.mroot.common.annotation.RequestLogAnnotation
import wang.encoding.mroot.common.constant.RequestLogConstant
import wang.encoding.mroot.common.exception.ControllerException
import wang.encoding.mroot.model.entity.system.User
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.cms.ArticleService
import wang.encoding.mroot.service.system.LoggingEventService
import wang.encoding.mroot.service.system.RequestLogService
import wang.encoding.mroot.service.system.UserService
import javax.servlet.http.HttpServletRequest

/**
 * 主页控制器
 *
 * @author ErYang
 */
@RestController
class IndexController : BaseAdminController() {

    companion object {

        /**
         * 模块
         */
        private const val MODULE_NAME: String = "/index"

        private const val INDEX_URL: String = "/index"
        private const val INDEX_VIEW: String = MODULE_NAME + INDEX_URL


        /**
         * 登录 url
         */
        private const val LOGIN_URL = "/"

        /**
         * 登录页面
         */
        private const val LOGIN_VIEW = "/login/login"
        /**
         * 登录页面 开发环境
         */
        private const val DEV_LOGIN_VIEW = "/login/login2Dev"

    }

    // -------------------------------------------------------------------------------------------------

    @Autowired
    private lateinit var computerHardwareUtil: ComputerHardwareUtil

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var requestLogService: RequestLogService

    @Autowired
    private lateinit var loggingEventService: LoggingEventService

    @Autowired
    private lateinit var articleService:ArticleService



    /**
     * 登录页面
     *
     * @return ModelAndView
     */
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE, title = RequestLogConstant.ADMIN_MODULE_LOGIN_VIEW)
    @RequestMapping(LOGIN_URL)
    @FormToken(init = true)
    @Throws(ControllerException::class)
    fun index(): ModelAndView {
        val modelAndView = ModelAndView()
        if (this.validationVerifyCode()) {
            modelAndView.addObject("isVerifyCode", "show")
        } else {
            modelAndView.addObject("isVerifyCode", "hide")
        }
        // 开发环境
        return if (super.devProfile()) {
            modelAndView.viewName = super.initView(DEV_LOGIN_VIEW)
            modelAndView
        } else {
            modelAndView.viewName = super.initView(LOGIN_VIEW)
            modelAndView
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 主页页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [INDEX_URL])
    @RequestMapping(INDEX_URL)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_INDEX_VIEW)
    @Throws(ControllerException::class)
    fun index(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(INDEX_VIEW))
        super.initViewTitleAndModelUrl(INDEX_URL, MODULE_NAME, request)

        val map: Map<String, Any> = computerHardwareUtil.osInfo()
        modelAndView.addObject("osInfoMap", map)

        // 用户数
        val userWhereMap: Map<String, Any> = mapOf(User.STATUS to StatusEnum.NORMAL.key)
        val userCount: Int = userService.count(userWhereMap)!!
        request.setAttribute("userCount", userCount)

        // 文章数
        val articleWhereMap: Map<String, Any> = mapOf()
        val articleCount: Int = articleService.count(articleWhereMap)!!
        request.setAttribute("articleCount", articleCount)

        // 日志数
        val logWhereMap: Map<String, Any> = mapOf()
        val logCount: Int = loggingEventService.count(logWhereMap)!!
        request.setAttribute("logCount", logCount)

        // 请求记录数
        val requestLogWhereMap: Map<String, Any> = mapOf()
        val requestLogCount: Int = requestLogService.count(requestLogWhereMap)!!
        request.setAttribute("requestLogCount", requestLogCount)

        request.setAttribute(INDEX_URL_NAME, contextPath + MODULE_NAME)
        request.setAttribute(NAV_INDEX_URL_NAME, contextPath + MODULE_NAME)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 检验是否需要验证码
     *
     * @return  Boolean
     */
    protected fun validationVerifyCode(): Boolean {
        val isVerifyCode: String? = ShiroSessionUtil.getAttribute("isVerifyCode") as String?
        return !(null == isVerifyCode || "show" != isVerifyCode)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End IndexController class

/* End of file IndexController.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/controller/index/IndexController.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
