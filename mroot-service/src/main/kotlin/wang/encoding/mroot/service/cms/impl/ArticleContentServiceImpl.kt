/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.cms.impl


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import wang.encoding.mroot.mapper.cms.ArticleContentMapper
import wang.encoding.mroot.model.entity.cms.ArticleContent
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.cms.ArticleContentService
import java.math.BigInteger
import java.time.Instant
import java.util.*


/**
 * 后台 文章内容 Service 接口实现类
 *
 * @author ErYang
 */
@Service
class ArticleContentServiceImpl : BaseServiceImpl
<ArticleContentMapper, ArticleContent>(), ArticleContentService {

    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(ArticleContentServiceImpl::class.java)
    }


    /**
     * 初始化新增 ArticleContent 对象
     *
     * @param articleContent ArticleContent
     * @return ArticleContent
     */
    override fun initSaveArticleContent(articleContent: ArticleContent): ArticleContent {
        articleContent.status = StatusEnum.NORMAL.key
        articleContent.gmtCreate = Date.from(Instant.now())
        return articleContent
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 ArticleContent 对象
     *
     * @param articleContent ArticleContent
     * @return ArticleContent
     */
    override fun initEditArticleContent(articleContent: ArticleContent): ArticleContent {
        articleContent.gmtModified = Date.from(Instant.now())
        return articleContent
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    override fun validationArticleContent(articleContent: ArticleContent): String? {
        return HibernateValidationUtil.validateEntity(articleContent)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 文章内容
     *
     * @param articleContent ArticleContent
     * @return ID  BigInteger
     */
    override fun saveBackId(articleContent: ArticleContent): BigInteger? {
        val id: Int? = super.save(articleContent)
        if (null != id && 0 < id) {
            return articleContent.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章内容(更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun removeBackId(id: BigInteger): BigInteger? {
        val articleContent = ArticleContent()
        articleContent.id = id
        articleContent.status = StatusEnum.DELETE.key
        articleContent.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.remove2StatusById(articleContent)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 文章内容 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun recoverBackId(id: BigInteger): BigInteger? {
        val articleContent = ArticleContent()
        articleContent.id = id
        articleContent.status = StatusEnum.NORMAL.key
        articleContent.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.recover2StatusById(articleContent)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章内容
     *
     * @param articleContent ArticleContent
     * @return ID  BigInteger
     */
    override fun updateBackId(articleContent: ArticleContent): BigInteger? {
        val flag: Boolean = super.updateById(articleContent)
        if (flag) {
            return articleContent.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleContentServiceImpl class

/* End of file ArticleContentServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/cms/impl/ArticleContentServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
