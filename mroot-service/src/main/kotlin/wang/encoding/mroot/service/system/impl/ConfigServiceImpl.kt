/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl


import com.baomidou.mybatisplus.plugins.Page
import com.baomidou.mybatisplus.plugins.pagination.Pagination
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.cache.annotation.CacheConfig
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import wang.encoding.mroot.mapper.system.ConfigMapper
import wang.encoding.mroot.model.entity.system.Config
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.system.ConfigService
import java.math.BigInteger
import java.time.Instant
import java.util.*
import javax.cache.Cache


/**
 * 后台 系统配置 Service 接口实现类
 *
 * @author ErYang
 */
@Service
@CacheConfig(cacheNames = ["configCache"])
class ConfigServiceImpl : BaseServiceImpl
<ConfigMapper, Config>(), ConfigService {

    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(ConfigServiceImpl::class.java)
    }

    @Value("\${spring.datasource.url}")
    private val datasourceUrl: String? = null


    @Autowired
    //@Qualifier("configCache")
    private lateinit var configCache: Cache<Any, Any>

    /**
     * 初始化新增 Config 对象
     *
     * @param config Config
     * @return Config
     */
    override fun initSaveConfig(config: Config): Config {
        config.status = StatusEnum.NORMAL.key
        config.gmtCreate = Date.from(Instant.now())
        return config
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 Config 对象
     *
     * @param config Config
     * @return Config
     */
    override fun initEditConfig(config: Config): Config {
        config.gmtModified = Date.from(Instant.now())
        return config
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    override fun validationConfig(config: Config): String? {
        return HibernateValidationUtil.validateEntity(config)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 查询 Config 缓存
     *
     * @param id ID
     * @return Config
     */
    @Cacheable(key = "#id")
    override fun getById2Cache(id: BigInteger): Config? {
        return super.getById(id)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 config
     *
     * @param name 标识
     * @return Config
     */
    @Cacheable(key = "#name")
    override fun getByName(name: String): Config? {
        val config = Config()
        config.name = name
        return super.getByModel(config)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 config
     *
     * @param title 名称
     * @return Config
     */
    @Cacheable(key = "#title")
    override fun getByTitle(title: String): Config? {
        val config = Config()
        config.title = title
        return super.getByModel(config)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 系统配置
     *
     * @param config Config
     * @return ID  BigInteger
     */
    override fun saveBackId(config: Config): BigInteger? {
        val id: Int? = super.save(config)
        if (null != id && 0 < id) {
            return config.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 系统配置(更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun removeBackId(id: BigInteger): BigInteger? {
        val config = Config()
        config.id = id
        config.status = StatusEnum.DELETE.key
        config.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.remove2StatusById(config)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 系统配置 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun recoverBackId(id: BigInteger): BigInteger? {
        val config = Config()
        config.id = id
        config.status = StatusEnum.NORMAL.key
        config.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.recover2StatusById(config)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 系统配置
     *
     * @param config Config
     * @return ID  BigInteger
     */
    override fun updateBackId(config: Config): BigInteger? {
        val flag: Boolean = super.updateById(config)
        if (flag) {
            return config.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 Config 缓存
     *
     * @param id BigInteger
     *
     */
    override fun removeCacheById(id: BigInteger) {
        val config: Config? = this.getById(id)
        if (null != config) {
            configCache.remove(config.id)
            configCache.remove(config.name)
            configCache.remove(config.title)

        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 批量移除　Config　缓存
     *
     * @param idArray ArrayList<BigInteger>
     *
     */
    override fun removeBatchCacheById(idArray: ArrayList<BigInteger>) {
        if (idArray.isNotEmpty()) {
            for (id: BigInteger in idArray) {
                this.removeCacheById(id)
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 用于分页 代码生成器专用
     *
     * @param page Page
     * @param tableArray ArrayList<String>
     * @return 集合
     */
    override fun listTable2PageByMap(page: Page<Map<String, String>>,
                                     tableArray: ArrayList<String>?): Page<Map<String, String>>? {
        val params = HashMap<String, Any>()
        params["dbName"] = getDbName()
        if (null != tableArray) {
            params["tableArray"] = tableArray
        }
        page.records = superMapper?.listTableByMap(params, page)
        return page
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件 得到表集合 代码生成器专用
     *
     * @param map 集合
     * @return 集合
     */
    override fun listTableByMap(map: HashMap<String, Any>): List<Map<String, String>>? {
        map["dbName"] = getDbName()
        return superMapper?.listTableByMap(map, Pagination())
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件 得到表详情 代码生成器专用
     *
     * @param tableName 表名
     * @return 集合
     */
    override fun getTableByTableName(tableName: String): List<Map<String, String>>? {
        val params = HashMap<String, String>()
        params["tableName"] = tableName
        params["dbName"] = getDbName()
        return superMapper?.getTableByTableName(params)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件 得到表详情 代码生成器专用
     *
     * @param tableName 表名
     * @return 集合
     */
    override fun getTableDetailTableName(tableName: String): List<Map<String, String>>? {
        val params = HashMap<String, String>()
        params["tableName"] = tableName
        params["dbName"] = getDbName()
        return superMapper?.getTableDetailByMap(params)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到数据库名称
     */
    private fun getDbName(): String {
        val url: String = datasourceUrl!!.substring(0, datasourceUrl.indexOf("?") + 1)
        return url.substring(url.lastIndexOf("/") + 1, datasourceUrl.indexOf("?"))
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigServiceImpl class

/* End of file ConfigServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/system/impl/ConfigServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
