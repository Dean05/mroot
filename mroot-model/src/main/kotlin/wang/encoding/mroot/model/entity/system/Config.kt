/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]
<http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>
<http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.system


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import org.hibernate.validator.constraints.Range
import java.io.Serializable

import java.math.BigInteger
import java.util.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern

/**
 * 系统配置实体类
 *
 * @author ErYang
 */
@TableName("system_config")
class Config : Model<Config>(), Serializable {

    companion object {

        private const val serialVersionUID = -8323888304661014387L

        /* 属性名称常量开始 */

        // -------------------------------------------------------------------------------------------------

        /**
         *表名
         */
        const val TABLE_NAME: String = "system_config"

        /**
         *表前缀
         */
        const val TABLE_PREFIX: String = "system_"

        /**
         * ID
         */
        const val ID: String = "id"

        /**
         * 类型(1是内容,2是表达式)
         */
        const val TYPE: String = "type"

        /**
         * 标识
         */
        const val NAME: String = "name"

        /**
         * 名称
         */
        const val TITLE: String = "title"

        /**
         * 内容
         */
        const val VALUE: String = "value"

        /**
         * 状态(1是正常，2是禁用，3是删除)
         */
        const val STATUS: String = "status"

        /**
         * 创建时间
         */
        const val GMT_CREATE: String = "gmt_create"

        /**
         * 创建IP
         */
        const val GMT_CREATE_IP: String = "gmt_create_ip"

        /**
         * 修改时间
         */
        const val GMT_MODIFIED: String = "gmt_modified"

        /**
         * 排序
         */
        const val SORT: String = "sort"

        /**
         * 备注
         */
        const val REMARK: String = "remark"

        // -------------------------------------------------------------------------------------------------

        /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param config  Config
         * @return Config
         */
        fun copy2New(config: Config): Config {
            val newConfig = Config()
            newConfig.id = config.id
            newConfig.type = config.type
            newConfig.name = config.name
            newConfig.title = config.title
            newConfig.value = config.value
            newConfig.status = config.status
            newConfig.gmtCreate = config.gmtCreate
            newConfig.gmtCreateIp = config.gmtCreateIp
            newConfig.gmtModified = config.gmtModified
            newConfig.sort = config.sort
            newConfig.remark = config.remark
            return newConfig
        }

        // -------------------------------------------------------------------------------------------------

    }


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    var id: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 类型(1是内容,2是表达式)
     */
    @NotNull(message = "validation.type.range")
    @Range(min = 1, max = 4, message = "validation.type.range")
    var type: Int? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 标识
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.name.pattern")
    var name: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 名称
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    var title: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 内容
     */
    @Pattern(regexp = "^[a-zA-Z0-9_{}\"':,./]{1,255}$", message = "validation.system.config.value.pattern")
    var value: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 状态(1是正常，2是禁用，3是删除)
     */
    @NotNull(message = "validation.status.range")
    @Range(min = 1, max = 3, message = "validation.status.range")
    var status: Int? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    var gmtCreate: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建IP
     */
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var gmtCreateIp: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    var gmtModified: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 排序
     */
    @Range(min = 0, message = "validation.sort.range")
    var sort: Long? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 备注
     */
    @Pattern(regexp = "^[a-zA-Z0-9，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    var remark: String? = null

    // -------------------------------------------------------------------------------------------------

    override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Config

        if (id != other.id) return false
        if (type != other.type) return false
        if (name != other.name) return false
        if (title != other.title) return false
        if (value != other.value) return false
        if (status != other.status) return false
        if (gmtCreate != other.gmtCreate) return false
        if (gmtCreateIp != other.gmtCreateIp) return false
        if (gmtModified != other.gmtModified) return false
        if (sort != other.sort) return false
        if (remark != other.remark) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (type ?: 0)
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (value?.hashCode() ?: 0)
        result = 31 * result + (status ?: 0)
        result = 31 * result + (gmtCreate?.hashCode() ?: 0)
        result = 31 * result + (gmtCreateIp?.hashCode() ?: 0)
        result = 31 * result + (gmtModified?.hashCode() ?: 0)
        result = 31 * result + (sort?.hashCode() ?: 0)
        result = 31 * result + (remark?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Config(id=$id, type=$type, name=$name, title=$title, value=$value, status=$status, gmtCreate=$gmtCreate, gmtCreateIp=$gmtCreateIp, gmtModified=$gmtModified, sort=$sort, remark=$remark)"
    }


    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End Config class

/* End of file Config.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/system/Config.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
