/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.system


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.FieldStrategy
import com.baomidou.mybatisplus.enums.IdType
import org.hibernate.validator.constraints.Range
import java.io.Serializable
import java.math.BigInteger
import java.util.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern

/**
 * 用户实体类
 *
 * @author ErYang
 */
@TableName("system_user")
class User : Model<User>(), Serializable {

    companion object {

        private const val serialVersionUID = 6302440471615851599L

        const val ID: String = "id"

        const val USERNAME: String = "username"

        const val NICK_NAME: String = "nick_name"

        const val STATUS: String = "status"

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param user  User
         * @return User
         */
        fun copy2New(user: User): User {
            val newUser = User()
            newUser.id = user.id
            newUser.type = user.type
            newUser.username = user.username
            newUser.nickName = user.nickName
            newUser.password = user.password
            newUser.email = user.email
            newUser.phone = user.phone
            newUser.realName = user.realName
            newUser.gmtCreate = user.gmtCreate
            newUser.gmtCreateIp = user.gmtCreateIp
            newUser.gmtModified = user.gmtModified
            newUser.status = user.status
            newUser.avatar = user.avatar
            return newUser
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    var id: BigInteger? = null
    /**
     * 用户类型(1是管理员，2是普通用户)
     */
    @NotNull(message = "validation.type.range")
    @Range(min = 1, max = 10, message = "validation.system.user.type.range")
    var type: Int? = null
    /**
     * 用户名
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{5,18}$", message = "validation.system.user.username.pattern")
    var username: String? = null
    /**
     * 昵称
     */
    @TableField("nick_name")
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{0,18}$", message = "validation.system.user.nickName.pattern")
    var nickName: String? = null
    /**
     * 密码
     */
    @Pattern(regexp = "^[0-9a-zA-Z]{6,18}$", message = "validation.system.user.password.pattern")
    var password: String? = null
    /**
     * 电子邮箱
     */
    @Pattern(regexp = "^([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)){0,50}$",
            message = "validation.system.user.email.pattern")
    var email: String? = null
    /**
     * 手机号码
     */
    @Pattern(regexp = "^(1[3,5,8][0-9][0-9]{8}){0,11}$", message = "validation.system.user.phone.pattern")
    var phone: String? = null
    /**
     * 真实姓名
     */
    @TableField("real_name")
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]{0,50}$", message = "validation.system.user.realName.pattern")
    var realName: String? = null
    /**
     * 创建时间
     */
    // fill = FieldFill.INSERT
    @TableField("gmt_create")
    //@Future(message = "validation.gmtCreate.future")
    @Past(message = "validation.gmtCreate.past")
    var gmtCreate: Date? = null
    /**
     * 创建 ip
     */
    @TableField("gmt_create_ip")
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var gmtCreateIp: String? = null
    /**
     * 修改时间
     */
    // fill = FieldFill.UPDATE
    @TableField(value = "gmt_modified", strategy = FieldStrategy.NOT_EMPTY)
    @Past(message = "validation.gmtModified.past")
    var gmtModified: Date? = null
    /**
     * 状态(1是正常，2是禁用，3是删除)
     */
    @NotNull(message = "validation.status.range")
    @Range(min = 1, max = 3, message = "validation.status.range")
    var status: Int? = null
    /**
     * 头像
     */
    var avatar: String? = null

    override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (id != other.id) return false
        if (type != other.type) return false
        if (username != other.username) return false
        if (nickName != other.nickName) return false
        if (password != other.password) return false
        if (email != other.email) return false
        if (phone != other.phone) return false
        if (realName != other.realName) return false
        if (gmtCreate != other.gmtCreate) return false
        if (gmtCreateIp != other.gmtCreateIp) return false
        if (gmtModified != other.gmtModified) return false
        if (status != other.status) return false
        if (avatar != other.avatar) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (type ?: 0)
        result = 31 * result + (username?.hashCode() ?: 0)
        result = 31 * result + (nickName?.hashCode() ?: 0)
        result = 31 * result + (password?.hashCode() ?: 0)
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + (phone?.hashCode() ?: 0)
        result = 31 * result + (realName?.hashCode() ?: 0)
        result = 31 * result + (gmtCreate?.hashCode() ?: 0)
        result = 31 * result + (gmtCreateIp?.hashCode() ?: 0)
        result = 31 * result + (gmtModified?.hashCode() ?: 0)
        result = 31 * result + (status ?: 0)
        result = 31 * result + (avatar?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "User(id=$id, type=$type, username=$username, nickName=$nickName, password=$password, email=$email, phone=$phone, realName=$realName, gmtCreate=$gmtCreate, gmtCreateIp=$gmtCreateIp, gmtModified=$gmtModified, status=$status, avatar=$avatar)"
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End User class

/* End of file User.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/system/User.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
