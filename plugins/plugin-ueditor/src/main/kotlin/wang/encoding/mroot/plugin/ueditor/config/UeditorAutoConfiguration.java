/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor.config;

import javax.annotation.PostConstruct;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import wang.encoding.mroot.common.constant.DatabaseConst;
import wang.encoding.mroot.common.constant.QiNiuConst;
import wang.encoding.mroot.common.util.QiNiuUtil;
import wang.encoding.mroot.model.entity.system.Config;
import wang.encoding.mroot.model.enums.ConfigTypeEnum;
import wang.encoding.mroot.model.enums.StatusEnum;
import wang.encoding.mroot.plugin.ueditor.ActionEnter;
import wang.encoding.mroot.plugin.ueditor.ConfigManager;
import wang.encoding.mroot.plugin.ueditor.hunter.FileManager;
import wang.encoding.mroot.plugin.ueditor.upload.StorageManager;
import wang.encoding.mroot.service.system.ConfigService;

import java.util.Map;


/**
 * 配置
 *
 * @author ErYang
 */
@Configuration
@EnableConfigurationProperties(UeditorProperties.class)
@ConditionalOnClass(ActionEnter.class)
@ConditionalOnProperty(prefix = "ueditor", value = "enabled", matchIfMissing = true)
public class UeditorAutoConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(UeditorAutoConfiguration.class);


    private final UeditorProperties ueditorProperties;

    private final ConfigService configService;

    private final QiNiuConst qiNiuConst;

    private final DatabaseConst databaseConst;

    @Autowired
    public UeditorAutoConfiguration(UeditorProperties ueditorProperties,
                                    DatabaseConst databaseConst, ConfigService configService,
                                    QiNiuConst qiNiuConst) {
        this.ueditorProperties = ueditorProperties;
        this.databaseConst = databaseConst;
        this.configService = configService;
        this.qiNiuConst = qiNiuConst;
    }

    /**
     * 注册 bean
     *
     * @return ActionEnter
     */
    @Bean
    @ConditionalOnMissingBean(ActionEnter.class)
    public ActionEnter actionEnter() {
        return new ActionEnter(new ConfigManager(ueditorProperties.getConfig()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 注入配置
     */
    @PostConstruct
    public void storageManager() {
        //ApplicationContext applicationContext = SpringContextUtil.getSpringContextUtil().getApplicationContext();
        //ConfigService configService = (ConfigService) applicationContext.getBean("configServiceImpl");
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置百度编辑器插件的七牛配置开始<<<<<<<<");
        }
        // 是否启用七牛cdn
        Boolean qiNiuCdn = false;
        // 得到数据库配置的上传配置
        Config config = configService.getByName(databaseConst.qiniuCdnName);
        if (null != config) {
            if (ConfigTypeEnum.FUN.getKey() == config.getType()) {
                if (null != config.getValue() && StringUtils.isNotBlank(config.getValue())) {
                    Map<String, String> map = null;
                    try {
                        // 得到json数据
                        map = (Map<String, String>) JSON.parse(config.getValue());
                    } catch (Exception e) {
                        if (logger.isErrorEnabled()) {
                            logger.error(">>>>>>>>得到配置JSON数据异常<<<<<<<<");
                            logger.error(e.getMessage());
                        }
                    }
                    if (null != map && !map.isEmpty()) {
                        String status = map.get("status");
                        if (null != status && String.valueOf(StatusEnum.NORMAL.getKey()).equals(status)) {
                            qiNiuCdn = true;
                        }
                    }
                }
            }
        }
        StorageManager.accessKey = FileManager.accessKey = qiNiuConst.accessKey;
        StorageManager.secretKey = FileManager.secretKey = qiNiuConst.secretKey;
        StorageManager.baseUrl = FileManager.baseUrl = qiNiuConst.baseUrl;
        StorageManager.bucket = FileManager.bucket = qiNiuConst.bucket;
        StorageManager.uploadDirPrefix = FileManager.uploadDirPrefix = qiNiuConst.uploadDirPrefix;
        StorageManager.zone = FileManager.zone = QiNiuUtil.ZONE;
        StorageManager.uploadLocal = !qiNiuCdn;
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置百度编辑器插件的七牛配置结束<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End UeditorAutoConfiguration class

/* End of file UeditorAutoConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/plugin/ueditor/config/UeditorAutoConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
